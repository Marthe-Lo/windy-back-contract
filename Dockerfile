FROM node:16.13.0

ENV NODE_ENV=production

COPY package*.json ./

RUN npm install --production

COPY . .

CMD [ "node", "index.js" ]

#CMD gunicorn app:app --bind 0.0.0.0:$PORT --reload