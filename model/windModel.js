const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const windSchema = new Schema({
  city_id: String,
  city_name: String,
  city_address: String,
  city_index: Number,
  city_latitude: Number,
  city_longitude: Number,
  city_index: Number,
  city_timeZone: String,

  temperature: Number,
  temperatureMax: Number,
  temperatureMin: Number,
  visibility: Number,

  windSpeed: Number,
  windGust: Number,
  windchill: Number,

  datetimeStr: Date,
  datetime: Date,
  cloudcover: Number,
  precip: Number,
  weathertype: String,
  humidity: Number,
  conditions: String,
  alerts: String,
});

const Wind = mongoose.model("Wind", windSchema);
module.exports = Wind;
