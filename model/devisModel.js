const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const devisSchema = new Schema({

  devisReference: {
    type: String,
    required: false,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  emailAddress: {
    type: String,
    required: false,
  },
  phoneNumber: { 
    type: String,
    required: false,
  },
  companyName: {
    type: String,
    required: false,
  },
  companyAddress: {
    type: String,
    required: true,
  },
 
  cityRiskName: {
    type: String,
    required: true,
  },
  windTurbineNumber: {
    type: Number,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
});

const Devis = mongoose.model("Devis", devisSchema);
module.exports = Devis;
