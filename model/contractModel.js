const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const contractSchema = new Schema({
  contractReference: {
    type: String,
    required: false
  },
  inceptionDate: {
    //date de création
    type: String,
    required: false,
  },
  emailAddress: {
    type: String,
    required: false,
  },
  companyName: {
    type: String,
    required: true,
  },
  coverageName: {
    type: String,
    required: false,
  },
  windSpeedThresholdCoverage: {
    type: Number,
    required: false,
  },
  cityRiskName: {
    type: String,
    required: true,
  },
  longitude: {
    type: Number,
    required: false,
  },
  latitude: {
    type: Number,
    required: false,
  },

  // Période de couverture
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  thirdPartyMeteoStation: {
    type: String,
    required: false,
  },
  InsuredCapitalPerTurbine: {
    type: Number,
    required: false,
  },
  windTurbineNumber: {
    type: Number,
    required: true,
  },
  daylyIndemnityPerWindTurbine: {
    type: Number,
    required: false,
  },
  numberOfDayOfIndemnity: {
    type: Number,
    required: false,
  },
  // maxIndemnity: {
  //   type: Number,
  //   required: false,
  // },
  // premiumRate: {
  //   type: Number,
  //   required: false,
  // },
  // premium: {
  //   type: Number,
  //   required: false,
  // },

  LastUpdate: {
    type: Date,
    default: Date.now,
    required: false,
  },

  //   city_id: String,
  //   city_name: String,
  //   city_address: String,
  //   city_index: Number,
  //   city_latitude: Number,
  //   city_longitude: Number,
  //   city_index: Number,
  //   city_timeZone: String,
  //   contractSpeed: Number,
  //   contractGust: Number,
  //   contractchill: Number,
  //   datetimeStr: Date,
});
mongoose.set('debug', true);

const Contract = mongoose.model("Contract", contractSchema);
module.exports = Contract;






// {
//   "inceptionDate": "30/12/2020",
//   "companyName": "Marseille Sarl",
//   "coverageName": "assurance paramétrique en cas d'absence de vent",
//   "windSpeedThresholdCoverage": 9,
//   "cityRiskName": "Paris",
//   "longitude": 28.6189,
//   "latitude": -81.335,
//   "startDate": "2020-01-01",
//   "endDate": "2021-12-31",
//   "thirdPartyMeteoStation": "Visual Crossing",
//   "InsuredCapitalPerTurbine": 50000,
//   "windTurbineNumber": 1,
//   "daylyIndemnityPerWindTurbine": 150,
//   "numberOfDayOfIndemnity": 0,    
//   "maxIndemnity": 0,
//   "premiumRate": 7,
//   "premium": 0,
//   "LastUpdate": 5000
// }