const express = require("express");
const mongoose = require("mongoose"),
  config = require("config"),
  bodyParser = require("body-parser"),
  app = express();
  const cors = require("cors");
  const port = process.env.PORT || 3000;

/****************************************    MIDDLEWARES / ROUTES    ****************************************/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
// require("./middleware/cors")(app);

//var windRouter = require("./controller/windController");
//app.use(windRouter.windRouter);
const windController = require("./controller/windController");
app.use("/winds", windController);

const contractController = require("./controller/contractController");
app.use("/contracts", contractController);


const devisController = require("./controller/devisController");
app.use("/devis", devisController);


/****************************************   CONNEXION A MONGOOSE    ****************************************/

mongoose
  .connect(config.get("dbConfig"), { useNewUrlParser: true })
  .then(() => {
    console.log("Connexion à MongoDB réussie");
    // app.listen(config.get("port"), () => {
    //   console.log("server run on port : ", config.get("port"));
    // });
    app.listen(port, function (err) {
      if (!err) {
          console.log(`App started on port ${port}`);
      } else {
          console.log(err);
      }
  });
  })
  .catch((err) => {
    console.log("La connexion a échoué", err);
  });
