const express = require("express");
const WindRouter = express.Router();
const lodash = require("lodash");
const moment = require("moment");
const Wind = require("../model/windModel");

/**********************************************         CREATE A WINDCITY         **********************************************/

// WindRouter.post("/create", (req, res) => {
//   var windData = req.body;
//   const newWind = new Wind({
//     ...windData,
//     endDate: new Date(windData.endDate),
//     startDate: new Date(windData.startDate),
//   });

//   newWind.save((err, savedWind) => {
//     if (err) {
//       res.status(400).json({
//         message: "bad request",
//         error: err,
//       });
//     }
//     res.status(201).json({
//       message: "insert success",
//       data: savedWind,
//     });
//   });
// });


/**********************************************         GET ALL WINDCITIES         **********************************************/

WindRouter.get("/getAll", (req, res) => {
  Wind.find((err, winds) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Winds",
      data: winds,
    });
  });
});


/**********************************************         GET ONE SPECIFIC WINDCITY         **********************************************/

WindRouter.get("/getOne/:id", (req, res) => {
  const id = req.params.id;

  Wind.findOne({ _id: id }, (err, wind) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!wind) {
      res.status(200).json({
        message: "searched Wind not exist",
      });
    } else
      res.status(200).json({
        message: "searched Wind",
        data: wind,
      });
  });
});

/**********************************************         UPDATE A WINDCITY         **********************************************/

WindRouter.put("/updateOne", (req, res) => {
  const id = req.query.id;
  const wind = req.body;

  Wind.findOneAndUpdate({ _id: id }, wind, (err, windUpdated) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!windUpdated) {
      res.status(200).json({
        message: "Wind not exist",
      });
    } else
      res.status(201).json({
        message: "Wind updated !",
        data: windUpdated,
      });
  });
});

// /**********************************************         DELETE A WINDCITY         **********************************************/
WindRouter.delete("/deleteById/:id", (req, res) => {
  const idWind = req.params.id;
  const filter = { _id: idWind };
  Wind.findByIdAndDelete(filter, (err, deleteWind) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else
      res.status(201).json({
        message: "Wind deleted !",
        data: deleteWind,
      });
  });
});

// /**********************************************         DELETE ALL         **********************************************/
WindRouter.delete("/deleteAll", (req, res) => {
  const filter = {};
  Wind.deleteMany(filter, (err, deleteListWind) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else
      res.status(201).json({
        message: "Winds deleted !",
        data: deleteListWind,
      });
  });
});

// /**********************************************         GET WIND < 15km/heure + intervalle de 2 dates         **********************************************/

WindRouter.get("/getByConditions", (req, res) => {
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;
  const speed = req.query.speed;

  Wind.find({}, (err, listWinds) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }

    let filterWindBySpeedAndDate = listWinds;

    if (speed)
      filterWindBySpeedAndDate = listWinds.filter(
        (wind) => Number(wind.windSpeed) < Number(speed)
      );

    if (lodash.isEmpty(filterWindBySpeedAndDate)) {
      res.status(200).json({
        message: "Winds Filtred !",
        data: filterWindBySpeedAndDate,
      });
      return;
    }
    if (startDate) {
      filterWindBySpeedAndDate = filterWindBySpeedAndDate.filter((wind) =>
        moment(new Date(wind.datetimeStr)).isAfter(moment(new Date(startDate)))
      );
    }

    if (endDate) {
      filterWindBySpeedAndDate = filterWindBySpeedAndDate.filter((wind) =>
        moment(wind.datetimeStr).isBefore(moment(endDate))
      );
    }
    res.status(200).json({
      message: "Winds Filtred !",
      data: filterWindBySpeedAndDate,
    });
  });
});



module.exports = WindRouter;
