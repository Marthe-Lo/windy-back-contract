const express = require("express");
const ContractRouter = express.Router();
const { v4: uuidv4 } = require("uuid");
const lodash = require("lodash");
const moment = require("moment");
const Contract = require("../model/contractModel");
const Wind = require("../model/windModel");

/**********************************************         CREATE A CONTRACT         **********************************************/
ContractRouter.post("/create", (req, res) => {
  var contractData = req.body;
  contractData.contractReference = contractData.cityRiskName + uuidv4();
  const newContract = new Contract({
    ...contractData,
    endDate: new Date(contractData.endDate),
    startDate: new Date(contractData.startDate),
  });

  newContract.save((err, savedContract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(201).json({
      message: "insert success",
      data: savedContract,
    });
  });
});

/**********************************************         GET ALL CONTRACTS         **********************************************/
ContractRouter.get("/getAll", (req, res) => {
  Contract.find((err, contracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Contracts",
      data: contracts,
    });
  });
});

/**********************************************         GET ALL CONTRACTS  BY USEREMAILADDRESS        **********************************************/
ContractRouter.get("/getAllContractsByUserEmail/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;

  Contract.find({ emailAddress: emailAddress }, (err, contract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Contracts",
      data: contract,
    });
  });
});

/**********************************************         COUNT ALL CONTRACTS  BY USEREMAILADDRESS        **********************************************/
ContractRouter.get(
  "/countAllContractsByUserEmail/:emailAddress",
  (req, res) => {
    const emailAddress = req.params.emailAddress;

    Contract.countDocuments(
      { emailAddress: emailAddress },
      (err, nbContractUser) => {
        if (err) {
          res.status(400).json({
            message: "bad request",
            error: err,
          });
        }
        res.status(200).json({
          message: "List of Contracts  for a User",
          data: nbContractUser,
        });
      }
    );
  }
);

/**********************************************         COUNT CITIES  BY USER EMAILADDRESS        **********************************************/
// ContractRouter.get(
//   "/countCitiesByUserEmail/:emailAddress",
//   (req, res) => {
//     const emailAddress = req.params.emailAddress;
//     const cityRiskName = req.params.cityRiskName;

//     Contract.countDocuments(
//       { _id: emailAddress },
//       (err, cityRiskName) => {
//         if (err) {
//           res.status(400).json({
//             message: "bad request",
//             error: err,
//           });
//         }
//         res.status(200).json({
//           message: "List of Contracts  for a User",
//           data: cityRiskName,
//         });
//       }
//     );
//   }
// );



/**********************************************         COUNT ALL CITIES FOR A USER        **********************************************/
ContractRouter.get("/countNbCitiesbyEmail/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;
  Contract.aggregate(
    [
      {
        $group: {
          _id: "$emailAddress",
          total: { $sum: "$cityRiskName" },
        },
      },
      {
        $match: {
          // match <=> where ou find
          _id: emailAddress,
        },
      },
    ],
    (err, nbCitiesUserEmail) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      }
      res.status(200).json({
        message: "List of Cities for a User",
        data: nbCitiesUserEmail,
      });
    }
  );
});




/**********************************************         COUNT ALL EOLIENNES FOR A USER        **********************************************/
ContractRouter.get("/countNbEoliennesUser/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;
  Contract.aggregate(
    [
      {
        $group: {
          _id: "$emailAddress",
          total: { $sum: "$windTurbineNumber" },
        },
      },
      {
        $match: {
          // match <=> where ou find
          _id: emailAddress,
        },
      },
    ],
    (err, nbEoliennesUser) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      }
      res.status(200).json({
        message: "List of Contracts  for a User",
        data: nbEoliennesUser,
      });
    }
  );
});

/**********************************************         COUNT ALL EOLIENNES  BY USEREMAILADDRESS        **********************************************/
// ContractRouter.get("/countAllEolienneByUserEmail/:emailAddress", (req, res) => {
//   const emailAddress = req.params.emailAddress;

//   Contract.countDocuments({emailAddress: emailAddress},(err, nbEoliennesUser) => {
//     if (err) {
//       res.status(400).json({
//         message: "bad request",
//         error: err,
//       });
//     }
//     res.status(200).json({
//       message: "List of Contracts",
//       data: nbEoliennesUser,
//     });
//   });
// });

/**********************************************         GET ONE SPECIFIC CONTRACT         **********************************************/
ContractRouter.get("/getOne/:id", (req, res) => {
  const id = req.params.id;

  Contract.findOne({ _id: id }, (err, contract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!contract) {
      res.status(200).json({
        message: "searched Contract not exist",
      });
    } else
      res.status(200).json({
        message: "searched Contract",
        data: contract,
      });
  });
});

/**********************************************         GET ONE SPECIFIC CONTRACT BY CONTRACTREFERENCE         **********************************************/
ContractRouter.get("/getOneByRef/:contractReference", (req, res) => {
  const contractReference = req.params.contractReference;

  Contract.findOne(
    { _id: contractReference },
    (err, contract) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      } else if (!contract) {
        res.status(200).json({
          message: "searched Contract not exist",
        });
      } else
        res.status(200).json({
          message: "searched Contract",
          data: contract,
        });
    }
  );
});

/**********************************************         UPDATE A CONTRACT         **********************************************/
ContractRouter.put("/updateOne", (req, res) => {
  const id = req.query.id;
  const contract = req.body;

  console.log("contract : ", contract);
  Contract.findOneAndUpdate(
    { _id: contract._id },
    contract,
    (err, contractUpdated) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      } else if (!contractUpdated) {
        res.status(200).json({
          message: "Contract not exist",
        });
      } else
        res.status(201).json({
          message: "Contract updated !",
          data: contractUpdated,
        });
    }
  );
});

// /**********************************************         DELETE A CONTRACT         **********************************************/
ContractRouter.delete("/deleteById/:id", (req, res) => {
  const idContract = req.params.id;
  const filter = { _id: idContract };
  Contract.findByIdAndDelete(filter, (err, deleteContract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else
      res.status(201).json({
        message: "Contract deleted !",
        data: deleteContract,
      });
  });
});

// /**********************************************         DELETE A CONTRACT by CONTRACTREFERENCE        **********************************************/
ContractRouter.delete("/deleteByContractReference/:id", (req, res) => {
  const idContract = req.params.id;
  console.log("id : ", idContract);
  const filter = { contractReference: idContract };
  Contract.findByIdAndDelete(filter, (err, deleteContract) => {
    console.log("filter : ", filter);

    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else
      res.status(201).json({
        message: "Contract deleted !",
        data: deleteContract,
      });
  });
});

// /**********************************************         DELETE ALL         **********************************************/
ContractRouter.delete("/deleteAll", (req, res) => {
  const filter = {};
  Contract.deleteMany(filter, (err, deleteListContract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else
      res.status(201).json({
        message: "Contracts deleted !",
        data: deleteListContract,
      });
  });
});

// /**********************************************         GET CONTRACTS ACCORDING TO CRITERIAS  : cities       **********************************************/

ContractRouter.get("/contractByCity/:city", (req, res) => {
  const city = req.params.city;
  Contract.findOne({ cityRiskName: city }, (err, contract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!contract) {
      res.status(200).json({
        message: "searched Contract not exist",
      });
    } else
      res.status(200).json({
        message: "searched Contract",
        data: contract,
      });
  });
});

// /**********************************************         GET CONTRACT => filtrer selon intervalle de 2 dates         **********************************************/

ContractRouter.get("/getByConditions", (req, res) => {
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;
  const speed = req.query.speed;

  // console.log("1. liste de contract :" + listContracts);

  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }

    let filterContractBySpeedAndDate = listContracts;

    // if (speed)
    //   filterContractBySpeedAndDate = listContracts.filter(
    //     (contract) => Number(contract.contractSpeed) < Number(speed)
    //   );

    if (lodash.isEmpty(filterContractBySpeedAndDate)) {
      res.status(200).json({
        message: "Contracts Filtred !",
        data: filterContractBySpeedAndDate,
      });
      return;
    }
    if (startDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) =>
          moment(new Date(contract.datetimeStr)).isAfter(
            moment(new Date(startDate))
          )
      );
    }
    console.log("2. liste de contract :" + filterContractBySpeedAndDate);

    if (endDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) => moment(contract.datetimeStr).isBefore(moment(endDate))
      );
    }
    res.status(200).json({
      message: "Contracts Filtred !",
      data: filterContractBySpeedAndDate,
    });
    console.log("3. liste de contract :" + filterContractBySpeedAndDate);
  });
});

/**********************************************  NOMBRE DES  CONTRACTS         **********************************************/

ContractRouter.get("/nbreContracts", (req, res) => {
  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }

    res.status(200).json({
      message: "Nombre de Contracts !",
      data: listContracts.length,
    });
  });
});

/****************      Nombre de jours ou vent < windSpeedThresholdCoverage entre 2 dates start et end du contrat        **********************************************/

ContractRouter.get("/nbreJoursByContractAndSeuil", (req, res) => {
  const speed = req.query.speed;
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;

  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    let filterContractBySpeedAndDate = listContracts;

    if (speed)
      filterContractBySpeedAndDate = listContracts.filter(
        (contract) =>
          Number(contract.windSpeedThresholdCoverage) < Number(speed)
      );

    if (lodash.isEmpty(filterContractBySpeedAndDate)) {
      res.status(200).json({
        message: "Nombre de jours by speed ( contract )",
        data: filterContractBySpeedAndDate.length,
      });
      return;
    }
    if (startDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) =>
          moment(new Date(contract.startDate)).isAfter(
            moment(new Date(startDate))
          )
      );
    }

    if (endDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) => moment(contract.endDate).isBefore(moment(endDate))
      );
    }
    res.status(200).json({
      message: "Nombre de jours by speed (contract) ",
      data: filterContractBySpeedAndDate.length,
    });
  });
});

/**********************************************  Liste + Nombre de jours ou vent < windSpeedThresholdCoverage entre 2 dates start et end du contrat        **********************************************/

ContractRouter.get("/listEtNbreJoursByContractAndSeuil", (req, res) => {
  const speed = req.query.speed;
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;

  console.log("speed : ", speed);
  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    let filterContractBySpeedAndDate = listContracts;

    if (speed)
      filterContractBySpeedAndDate = listContracts.filter(
        (contract) =>
          Number(contract.windSpeedThresholdCoverage) < Number(speed)
      );

    if (lodash.isEmpty(filterContractBySpeedAndDate)) {
      res.status(200).json({
        message: "Nombre de jours by speed ( contract )",
        data: {
          length: filterContractBySpeedAndDate.length,
          list: filterContractBySpeedAndDate,
        },
      });
      return;
    }
    if (startDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) =>
          moment(new Date(contract.startDate)).isAfter(
            moment(new Date(startDate))
          )
      );
    }

    if (endDate) {
      filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
        (contract) => moment(contract.endDate).isBefore(moment(endDate))
      );
    }
    res.status(200).json({
      message: "Nombre de jours by speed (contract) ",
      data: {
        length: filterContractBySpeedAndDate.length,
        list: filterContractBySpeedAndDate,
      },
    });
  });
});





/**********************************************  nombre de jours d'indemnisation pour tous les contrats       **********************************************/

ContractRouter.get("/nbrDaysOfSettlementForAllContract", (req, res) => {
  const speedWind = req.params.speedWind;
  const speedContract = req.params.speedContract;
  const startDate = req.params.startDate;
  const endDate = req.params.endDate;

  Contract.aggregate(
    [
      { $unwind: "$cityRiskName" },
      { $unwind: "$startDate" },
      { $unwind: "$endDate" },
      { $unwind: "$windSpeedThresholdCoverage" },
      { $unwind: "$emailAddress" },

      {
        $lookup: {
          from: "winds",
          localField: "cityRiskName", //contrat = localField  $
          foreignField: "city_name", // winds = foreignField $$
          as: "matches",
        },
      },
      {
        $unwind: {
          path: "$matches",
        },
      },
      {
        $match: {
          $expr: {
            $and: [
              {
                $lte: ["$startDate", "$matches.datetimeStr"],
              },
              {
                $gte: ["$endDate", "$matches.datetimeStr"],
              },
              {
                $lte: [
                  "$matches.windSpeed", // wind
                  "$windSpeedThresholdCoverage", // contrat
                ],
              },
            ],
          },
        },
      },

      {
        $group: {
          _id: "$matches._id",
        },
      },
    ],
    (err, nbrDaysSettlement) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      }
      res.status(200).json({
        message: "nombre de jours d'indemnisation pour un contrat",
        data: nbrDaysSettlement.length,
      });
    }
  );
});

/**********************************************  nombre de jours d'indemnisation pour un contrat        **********************************************/

ContractRouter.get("/nbrDaysOfSettlementForOneContract/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;

  Contract.aggregate(
    [
     
      { $unwind: "$cityRiskName" },
      { $unwind: "$startDate" },
      { $unwind: "$endDate" },
      { $unwind: "$windSpeedThresholdCoverage" },
      { $unwind: "$emailAddress" },

      {
        $lookup: {
          from: "winds",
          localField: "cityRiskName", //contrat = localField  $
          foreignField: "city_name", // winds = foreignField $$
          as: "matches",
        },
      },
      {
        $unwind: {
          path: "$matches",
        },
      },
      {
        $match: {
          $expr: {
            $and: [
              {
                $eq : [
                 "$emailAddress",
                 emailAddress
                ]
              },
              {
                $lte: ["$startDate", "$matches.datetimeStr"],
              },
              {
                $gte: ["$endDate", "$matches.datetimeStr"],
              },
              {
                $lte: [
                  "$matches.windSpeed", // wind
                  "$windSpeedThresholdCoverage", // contrat
                ],
              },
            ],
          },
        },
      },
      {
        $group: {
          _id: "$matches._id",

        }
      }
    ],
    (err, nbrDaysSettlement) => {
      if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      }
      res.status(200).json({
        message: "nombre de jours d'indemnisation pour un contrat",
        data: nbrDaysSettlement.length,
      });
    }
  );
});

/**********************************************  Liste + Nombre de jours ou vent < windSpeedThresholdCoverage entre 2 dates start et end du contrat        **********************************************/

// ContractRouter.get("/listEtNbreJoursByContractAndSeuil", (req, res) => {
//   const speed = req.query.speed;
//   const startDate = req.query.startDate;
//   const endDate = req.query.endDate;

//   Contract.find({}, (err, listContracts) => {
//     if (err) {
//       res.status(400).json({
//         message: "bad request",
//         error: err,
//       });
//       return;
//     }

//     let filterContractBySpeedAndDate = listContracts;

//     if (speed)
//       filterContractBySpeedAndDate = listContracts.filter(
//         (contract) =>
//           Number(contract.windSpeedThresholdCoverage) < Number(speed)
//       );

//     if (lodash.isEmpty(filterContractBySpeedAndDate)) {
//       res.status(200).json({
//         message: "Nombre de jours by speed ( contract )",
//         data: {
//           length: filterContractBySpeedAndDate.length,
//           list: filterContractBySpeedAndDate,
//         },
//       });
//       return;
//     }
//     if (startDate) {
//       filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
//         (contract) =>
//           moment(new Date(contract.startDate)).isAfter(
//             moment(new Date(startDate))
//           )
//       );
//     }

//     if (endDate) {
//       filterContractBySpeedAndDate = filterContractBySpeedAndDate.filter(
//         (contract) => moment(contract.endDate).isBefore(moment(endDate))
//       );
//     }
//     res.status(200).json({
//       message: "Nombre de jours by speed (contract) ",
//       data: {
//         length: filterContractBySpeedAndDate.length,
//         list: filterContractBySpeedAndDate,
//       },
//     });
//   });
// });

/**********************************************  CONTRAT = critères (nbre de jours ou vent < windSpeedThresholdCoverage) +  intervalle de 2 dates start et end du contrat         **********************************************/

ContractRouter.get("/nbreJoursEtAndSeuilByContratindemnisable", (req, res) => {
  const speed = req.query.speed;
  const startDate = req.query.startDate;
  const endDate = req.query.endDate;
  const idContract = req.query.idContract;

  Contract.findOne({ _id: idContract }, (err, contract) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    if (!contract) {
      res.status(400).json({
        message: "Aucun contract trouvé",
        error: err,
      });
      return;
    }

    let isTrue = false;

    if (speed && Number(contract.windSpeedThresholdCoverage) < Number(speed))
      isTrue = true;
    if (isTrue && startDate) {
      if (
        moment(new Date(contract.startDate)).isAfter(
          moment(new Date(startDate))
        )
      )
        isTrue = true;
      else isTrue = false;
    }
    if (isTrue && endDate) {
      if (endDate && moment(contract.endDate).isBefore(moment(endDate)))
        isTrue = true;
      else isTrue = false;
    }

    if (isTrue)
      res.status(200).json({
        message: " Le contrat est indemnisable ",
        data: true,
      });
    else
      res.status(200).json({
        message: " Le contrat est non indemnisable ",
        data: false,
      });
  });
});

/**********************************************  récupérer le nombre d'éoliennes pour tous les contrats en base (du portefeuille)         **********************************************/

ContractRouter.get("/nbreEoliennes", (req, res) => {
  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    let nbreEol = 0;
    lodash.forEach(
      listContracts,
      (val) => (nbreEol = nbreEol + val.windTurbineNumber)
    );

    res.status(200).json({
      message: "Nombre d'éoliennes !",
      data: nbreEol,
    });
  });
});



/**********************************************  récupérer le nombre de villes pour tous les contrats (du portefeuille ou d'un client)        **********************************************/

ContractRouter.get("/nbreCityRiskName", (req, res) => {
  Contract.find({}, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    let nbreCityRiskName = 0;
    lodash.forEach(
      listContracts,
      (val) => (nbreCityRiskName = nbreCityRiskName + val.cityRiskName)
    );

    res.status(200).json({
      message: "Nombre de villes !",
      data: nbreCityRiskName.length,
    });
  });
});



/**********************************************  récupérer le nombre d'éoliennes pour UN contrat         **********************************************/

ContractRouter.get("/nbreEoliennesParContrat/:id", (req, res) => {
  const idContract = req.params.id;
  Contract.findOne({ _id: idContract }, (err, contract) => {
    if (err) {
      res.status(400).json({
        message: "contrat non trouvé",
        error: err,
      });
      return;
    }

    res.status(200).json({
      message: "Nombre d'éoliennes pour UN contrat !",
      data: contract.windTurbineNumber,
    });
  });
});



/**********************************************  récupérer le nombre d'éoliennes pour les contrats d'une ville         **********************************************/

ContractRouter.get("/nbreEoliennesByCityRisk", (req, res) => {
  const cityRisk = req.query.cityRisk;
  Contract.find({ cityRiskName: cityRisk }, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    if (lodash.isEmpty(listContracts)) {
      res.status(400).json({
        message: "la ville n'existe pas en bdd",
        error: err,
      });
      return;
    }
    let nbreEol = 0;
    lodash.forEach(
      listContracts,
      (val) => (nbreEol = nbreEol + val.windTurbineNumber)
    );

    res.status(200).json({
      message: "Nombre d'éoliennes By City Risk Name !",
      data: nbreEol,
    });
  });
});

/**********************************************  récupérer le nombre de companyName du portefeuille         **********************************************/

ContractRouter.get("/nbrecompanyName", (req, res) => {
  const companyN = req.query.companyN;
  Contract.find({ companyName: companyN }, (err, listContracts) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
      return;
    }

    if (lodash.isEmpty(listContracts)) {
      res.status(400).json({
        message: "le companyName n'existe pas en bdd",
        error: err,
      });
      return;
    }

    res.status(200).json({
      message: "Nombre de companyName !",
      data: listContracts.length,
    });
  });
});

// /**********************************************         GET CONTRACTS ACCORDING TO CRITERIAS  : contractspeed < 10km/h       **********************************************/

// // //  URL: http://localhost:6000/contractxxxxxx => returning all contracts if no ?prixMini
// // ContractRouter.route('/contract-api/public/contract')
// // .get( function(req , res , next ) {
// //     // var prixMini = req.query.prixMini;
// //     // var criteria=prixMini?{ prix : { $gte: prixMini } }:{};
// // 	PersistentContract.find(criteria,function(err,contracts){
// // 		   if(err) {
// // 			   console.log("err="+err);
// // 	       }
// // 		   res.send(contracts);
// // 	});//end of find()
// // });

// /**********************************************         GET CONTRACTS ACCORDING TO CRITERIAS  : contractspeed < 15km/h       **********************************************/

// // //  URL: http://localhost:6000/contractxxxxxx => returning all contracts if no ?prixMini
// // ContractRouter.route('/contract-api/public/contract')
// // .get( function(req , res , next ) {
// //     // var prixMini = req.query.prixMini;
// //     // var criteria=prixMini?{ prix : { $gte: prixMini } }:{};
// // 	PersistentContract.find(criteria,function(err,contracts){
// // 		   if(err) {
// // 			   console.log("err="+err);
// // 	       }
// // 		   res.send(contracts);
// // 	});//end of find()
// // });

module.exports = ContractRouter;
