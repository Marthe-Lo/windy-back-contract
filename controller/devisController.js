const express = require("express");
const DevisRouter = express.Router();
const lodash = require("lodash");
const Devis = require("../model/devisModel");



/**********************************************         CREATE A CONTRACT         **********************************************/
DevisRouter.post("/create", (req, res) => {
  var devisData = req.body;
  console.log("devis ", devisData);
  const newDevis = new Devis({
    ...devisData,
    endDate: new Date(devisData.endDate),
    startDate: new Date(devisData.startDate),
  });

  newDevis.save((err, savedDevis) => {
    console.log("erreur :", err);
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(201).json({
      message: "insert success",
      data: savedDevis,
    });
  });
});


/**********************************************         GET ALL DEVIS         **********************************************/
DevisRouter.get("/getAll", (req, res) => {
  Devis.find((err, devis) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Devis",
      data: devis,
    });
  });
});


/**********************************************         GET ALL DEVIS  BY USEREMAILADDRESS       **********************************************/
DevisRouter.get("/getAllDevisByUserEmail/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;

  Devis.find({emailAddress: emailAddress},(err, devis) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Devis",
      data: devis,
    });
  });
});


/**********************************************         COUNT ALL DEVIS  BY USEREMAILADDRESS        **********************************************/
DevisRouter.get("/countAllDevisByUserEmail/:emailAddress", (req, res) => {
  const emailAddress = req.params.emailAddress;

  Devis.countDocuments({emailAddress: emailAddress},(err, nbDevisByUser) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }
    res.status(200).json({
      message: "List of Devis for a User",
      data: nbDevisByUser,
    });
  });
});




/**********************************************     DEVIS :  COUNT ALL EOLIENNES FOR A USER        **********************************************/
DevisRouter.get(
  "/countnbEoliennesUser/:emailAddress",
  (req, res) => {
    const emailAddress = req.params.emailAddress;
    Devis.aggregate(
      [{$group: {
        _id: "$emailAddress",
        total: { $sum: "$windTurbineNumber" }
    }},
    {$match: {               // match <=> where ou find
      _id: emailAddress
  }}
      ],
      (err, nbEoliennesUser) => {
        if (err) {
          res.status(400).json({
            message: "bad request",
            error: err,
          });
        }
        res.status(200).json({
          message: "List of Contracts  for a User",
          data: nbEoliennesUser,
        });
      }
    );
  }
);




/**********************************************         GET ONE SPECIFIC CONTRACT         **********************************************/
DevisRouter.get("/getOne/:id", (req, res) => {
  const id = req.params.id;

  Devis.findOne({ _id: id }, (err, devis) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!devis) {
      res.status(200).json({
        message: "searched Devis not exist",
      });
    } else
      res.status(200).json({
        message: "searched Devis",
        data: devis,
      });
  });
});


/**********************************************         UPDATE A DEVIS         **********************************************/
DevisRouter.put("/updateOne", (req, res) => {
  const id = req.query.id;
  const devis = req.body;

  Devis.findOneAndUpdate({ _id: devis._id }, devis, (err, devisUpdated) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    } else if (!devisUpdated) {
      res.status(200).json({
        message: "Devis not exist",
      });
    } else
      res.status(201).json({
        message: "Devis updated !",
        data: devisUpdated,
      });
  });
});

// /**********************************************         DELETE A CONTRACT         **********************************************/
DevisRouter.delete("/deleteById/:id", (req, res) => {
  const idDevis = req.params.id;
  const filter = { _id: idDevis };
  Devis.findByIdAndDelete(filter, (err, deleteDevis) => {
    if (err) {
        res.status(400).json({
          message: "bad request",
          error: err,
        });
      } else
        res.status(201).json({
          message: "Devis deleted !",
          data: deleteDevis,
        });
  });
});


// /**********************************************         DELETE ALL         **********************************************/
DevisRouter.delete("/deleteAll", (req, res) => {
    const filter = {};
    Devis.deleteMany(filter, (err, deleteListDevis) => {
      if (err) {
          res.status(400).json({
            message: "bad request",
            error: err,
          });
        } else
          res.status(201).json({
            message: "Devis deleted !",
            data: deleteListDevis,
          });
    });
  });


  
/**********************************************  NOMBRE DE DEVIS         **********************************************/

DevisRouter.get("/nbreDevis", (req, res) => {
  Devis.find({}, (err, listDevis) => {
    if (err) {
      res.status(400).json({
        message: "bad request",
        error: err,
      });
    }

    res.status(200).json({
      message: "Nombre de Devis !",
      data: listDevis.length,
    });
  });
});


module.exports = DevisRouter;
